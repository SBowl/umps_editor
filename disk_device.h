#ifndef _H_DISK
#define _H_DISK

#define E_NOINST -1 /* device not installed error */
#define E_ILLOP -2  /* illegal operation error */
#define E_BUSY -3   /* disk busy error */
#define E_SEEK -4   /* seek error */
#define E_READ -5   /* read error */
#define E_WRITE -6  /* write error */
#define E_DMAT -7   /* DMA transfer error */
#define E_BNFR -8   /* block not free*/
#define E_BNUS -9   /* block not used */
#define E_NSPC -10  /* no space */

typedef struct disk_block{
    unsigned int cyl;
    unsigned int head;
    unsigned int sect;
    struct disk_block *next;
} disk_block_t; 

typedef struct dsk_repr{
    unsigned int disk_id;
    unsigned int max_cyl;
    unsigned int max_head;
    unsigned int max_sect;
    disk_block_t* blocks_head;
} disk_repr_t;

/*
    *Safely* write a block to the disk given as input.
    This function will never override a block, if the given block coordinates 
    collide with an already allocated block E_BNFR will be returned.

    If no free block is found on the entire disk, an error code will be returned. 

    If the write operation was successful then 'block' is appended to disk's list
    of blocks.
*/
int swblock(disk_repr_t * disk, disk_block_t* block, char *data, int data_length);

/*
    This function is used to write a block on a disk specifying its coordinates.
    Contrary to swblock this function allows to override an already allocated block.

*/
int wblock(disk_repr_t * disk, disk_block_t* block, char *data, int data_length);

/*
    Append block. Seeks the disk for a free block and writes the input
    data on it.
    The coordinates of the newly allocated block are written in the given
    disk_block_t structure.
    If no free blocks are found E_NSPC is returned.
*/
int ablock(disk_repr_t *disk, disk_block_t *coordinates, char *data, int data_length);

/*
    Removes a block from the disk.
    This function will both flag the physical block on disk as 'FREE' 
    (setting the block's first 2 bytes to 0xf50ee).
*/
int dblock(disk_repr_t * disk, disk_block_t * block);

/*
    Allows to update a block.
    The coordinates for the block to be updated are read from the disk_block_t
    structure passed as input.
    If the specified block is not an allocated block E_BNUS will be returned.
*/
int ublock(disk_repr_t * disk, disk_block_t* to_update, char *data, int data_length);

/*
    `Safely` reads the contents of a block to the specified buffer truncated to either 
    the block's size or 'max_length'.
    The block to be read is specified using the disk_block_t parameter.
    If the given block coordinates refer to a non-used block E_BNUS will be returned
    and no operation will be performed.
*/
int srblock(disk_repr_t *disk, disk_block_t* block, char *data_buffer, int max_length);

/*
    Reads a block from disk.
    The content of the block will be written to the buffer pointed
    by 'data_buffer' truncated to the smallest between 'max_length' and the block's
    size (~4 KB).
    The physical block to read is specified with the disk_block_t* parameter.
    Since this function performs an `unsafe` read a non-used block
    read will not be signaled.
*/
int rblock(disk_repr_t *disk, disk_block_t* block, char *data_buffer, int max_length);

/*
    Counts the used blocks on disk.
    Returns the number of used blocks found on the disk.
*/
int cblocks(disk_repr_t *disk);

/*
    Reads the geometry of the specified disk and writes it into a
    disk_block_t struct.
    E_NOINST is returned if no disk with the given id is found.
*/
int disk_geometry(disk_block_t * geom, int disk_id);

/*
    Scans the disk and looks for allocated physical blocks. Every allocated
    block's coordinates are written into a new disk_block_t from the list given
    as input. The integer parameter 'max_blocks' indicates the maximum number
    of blocks to use from the given list.
    Returns the number of retrieved blocks.
*/
int scan_disk(disk_repr_t *disk, disk_block_t blocks[], int max_blocks);

/*
    Initializes the disk device specified as input and stores
    its geometry to 'disk'.
*/
void init_disk(disk_repr_t *disk);

#endif