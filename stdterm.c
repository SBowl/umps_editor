#include "stdterm.h"
#include "umps/types.h"
#include "umps/arch.h"

#define PRINT_INT_LENGTH 20

static termreg_t *termreg = (termreg_t *) DEV_REG_ADDR(IL_TERMINAL, 0);

int __get_transm_status(){
    return termreg->transm_status & 0xff;
}

int __get_recv_status(){
    return termreg->recv_status & 0xff;
}

int print_char(char c){
    int result = 0;
    if(__get_transm_status() == TERM_READY){
        termreg->transm_command = (c << 8) | TERM_CMD_TRNS_RCV;
        while(__get_transm_status() == TERM_BUSY);
        termreg->transm_command = TERM_CMD_ACK; 
    }else{
        result = -1;
    }
    return result;
}

int print(char * c){
    int result = 0;
    while(*c != 0){
        if(print_char(*c)){
            return -1;
        }
        result++;
        c++;
    }
    return result;
}

int read_char(){
    int result = 0;
    if(__get_recv_status() == TERM_READY){
        termreg->recv_command = TERM_CMD_TRNS_RCV;
        while(__get_recv_status() == TERM_BUSY);
        if(__get_recv_status() == TERM_CHAR_RECVD_TRSMD){
            result = (termreg->recv_status >> 8) & 0xff;
            termreg->recv_command = TERM_CMD_ACK;
        }else{
            result = -1;
        }
    }else{
        result = -1;
    }
    return result;
}

int read_digit() {
    return read_char() - 48;
}

int read_string(char *s, int bound){
    int result = 0;
    while(result < bound){
        char *current = (s + result);
        *current = read_char();
        if (!*current || *current == '\n'){
            *current = 0;
            return result;
        }
        result++;
    }
    return result;
}

int print_int(int n){
    int i = 0;
    int copy = n;
    int result = 0;
    char buff[PRINT_INT_LENGTH];
    if(n < 0){
        n = -n;
    }else if(n == 0){
        print_char('0');
    }
    for(i = 0; i < PRINT_INT_LENGTH && n != 0; i++){
        buff[i] = (n % 10) + 48;
        n /= 10;
    }
    result = --i;
    if(copy < 0){
        print_char('-');
    }
    for(; i >= 0; i--){
        print_char(buff[i]);
    }
    return result;
}
