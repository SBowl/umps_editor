# UMPS disk driver
A library that allows a user to perform read/write operations to disks on the uMPS2 emulator.
The project is hosted on [GitLab](https://gitlab.com/SBowl/umps_editor "Repo").

## Table of Contents

- [UMPS disk driver](#umps-disk-driver)
  - [Table of Contents](#table-of-contents)
  - [Before you start](#before-you-start)
  - [Usage](#usage)
  - [Interface](#interface)
    - [Operations](#operations)
    - [Structures](#structures)
    - [Error codes](#error-codes)
  - [Demo](#demo)
    - [Warm Up](#warm-up)
    - [Part 1](#part-1)
    - [Part 2](#part-2)

## Before you start
This file was written with the purpose to give a fast high level
view of the things this library does, a more complete documentation
is available in the header file's comments.  
You also won't find anything about the implementation. For that you
should look at the code and comments in the __disk_device.c__ file.

## Usage
Before performing any operation on a disk it is necessary to initialize it:

```c
int disk_id = 0;
disk_repr_t disk = {disk_id, -1, -1, -1, 0};
initialize_disk(&disk);
```
After that it will be possible to use `disk` to perform any operation on the physical disk.  
Let's write a block:
```c
disk_block_t block = {0, 0, 1, 0}; // cyl 0, head 0, and sect 1
char data[] = "hello world";
int status = swblock(&disk, &block, data, 11);
if(status > 0){
    // no error
}else{
    // handle error
}
```
If all goes correctly, running hexdump on the disk file should return something like this:
```bash
00000000  4d 50 53 00 20 00 00 00  02 00 00 00 08 00 00 00  |MPS. ...........|
00000010  1a 41 00 00 64 00 00 00  50 00 00 00 80 57 68 65  |.A..d...P....Whe|
00000020  6c 6c 6f 20 77 6f 72 6c  64 00 00 00 00 00 00 00  |llo world.......|
00000030  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
```
Note that before the bytes for 'hello world' there is a two bytes flag 0x8057,
this indicates that the block is allocated and that its data is valid. This flag will be better
explained in the __Interface__ section.

## Interface
The interface contains functions that perform simple read/write operations
on a disk. To perform these operations two data structures ('disk_repr_t', 'disk_block_t') are given which will be later
discussed.  
Since this is meant to be a low level library to access disk drivers, no great level
of abstraction is provided if not for the concept of *free block* and *allocated block*. To tell
between these two types a 2 bytes flag is applied to each physical block that is written or 
deallocated __(0xf5ee for a free block and 0x8057 for an allocated one)__. This means that, assuming a block size of 4KB, 4094 bytes can be written into a block.

### Operations
We can distinguish two types of operations: *safe* and *unsafe*. Safe operations like swblock, srblock and ablock
generally make sure that a user does not write on an already allocated block (*overriding*) or read
a non-allocated block.  
While using a safe operation any 'infraction' will terminate the function and return
the appropriate error code.

Unsafe operations instead (wblock, rblock and others) give full control to the user, it will be possible to read 
any block coordinate and to write anywhere on the disk (as long as disk's gemoetry is being respected).

To summarize we can broadly say that safe operations act considering the information written
in the block flag, while unsafe operations don't look at any metadata stored in physical blocks.

### Structures
Two data structures have been defined to ease information passing:
1) __disk_repr_t__ is a representation of a disk device
```c
typedef struct dsk_repr{
    unsigned int disk_id;
    unsigned int max_cyl;
    unsigned int max_head;
    unsigned int max_sect;
    disk_block_t* blocks_head; // used to save the head of the allocated block list
} disk_repr_t;
```
2) __disk_block_t__ represents a physical block on a disk
```c
typedef struct disk_block{
    unsigned int cyl;
    unsigned int head;
    unsigned int sect;
    struct disk_block *next; // used to implement the block list
} disk_block_t; 
```
The fields are all self explanatory, only __disk_repr_t.blocks_head__ and __disk_block.next__ require some explanation:  
These two fields are used to create a block list which is associated with a specific disk, this use is intended
as a way to conveniently store all allocated blocks an user could create. A list of allocated blocks is also
useful for storing the result of a disk scansion.
Altough the data structures allow for the creation of these lists no function to do this is provided by this library.

### Error codes
Following is a list of all the error codes that can be returned from the functions.
-1 to -7 correspond to the errors described in the uMPS2 manual, the remaining ones
are used to signal invalid safe operations.
- uMPS2 disk error codes:
  - E_NOINST -1
  - E_ILLOP -2
  - E_BUSY -3
  - E_SEEK -4
  - E_READ -5
  - E_WRITE -6
  - E_DMAT -7
- Library defined error codes:
  - E_BNFR -8 (Block not free) returned on a write operation performed on a block that cannot be written.
  - E_BNUS -9 (Block not used) returned on read operation performed on a non allocated block.
  - E_NSPC -10 (No space) returned when no free blocks where found while seeking the whole disk.

## Demo
The demo has been created with the purpose to illustrate the capabilities of the library. It is divided in two parts:

1. Just a simple example of how you would use the library;
2. An interactive demo where user will be able to perform actions on disk using uMPS terminal.

### Warm Up
1. Create a disk file from your terminal (outside uMPS) with:

```bash
umps2-mkdev -d an_awesome_disk_name.umps
```

2. `cd` inside project directory and run `makefile` in order to compile the .core and .stab files;
3. Configure your machine in uMPS with the three files you just created. The disk file should be used as the device file for disk 0, make sure it's enabled before starting;
4. Run your machine, open it's terminal and follow the instructions;
5. Have fun!

### Part 1
This part does not require any input from the user. The program just executes some operations and describes them. See the code in `main.c` in order to understand how the operations are being executed.

The program should:
1. Initialize disk structure as a model of the actual disk (the one mounted in uMPS2 as disk 0 before starting the machine);
2. Print disk's geometry (max cylinder, max head and max sector);
3. Count the allocated blocks with `cblock` and print the result;
4. Scan the disk with `scan_disk`, print the allocated blocks and then print the coordinates of the first allocated block;
5. Append a new block containing *hello world* and then count the allocated blocks again;
6. Wait for user input in order to start part 2.

### Part 2
This is the main part of the demo, here the user will be asked to perform these actions (we recommend starting with number 9):

0. Safely write a block with `swblock` after asking user for block's coordinates and data to store;
1. Write a block with `wblock` after asking user for block's coordinates and data to store;
2. Prompts the user for data and stores it inside the first free block, using `ablock`;
3. Updates a block with `ublock` after asking user for block's coordinates and data to store;
4. Deletes a block with `dblock` after asking user for block's coordinates;
5. Safely reads a block with `srblock` and prints the results after asking user for block's coordinates;
6. Reads a block with `rblock` and prints the results after asking user for block's coordinates;
7. Counts allocated blocks with `cblocks` and prints the result;
8. Prints disk's geometry (max cylinder, max head, max sector);
9. Scans disk with `scan_disk` and prints each allocated block's coordinates.

Reboot the machine after performing some actions and notice that everything you wrote has been stored.