#ifndef _H_ACTIONS
#define _H_ACTIONS

#include "disk_device.h"

/**
 * @param {char*} c : string to be converted
 * @param {unsigned int} length : string length
 * 
 * @returns {int} the converted value
 * 
 * Takes a string and its length as params,
 * converts the string to an int
 * and returns the converted value.
*/
int parse_string_to_int(char *c, unsigned int length);

/**
 * @param {int} code : possible error code value defined in disk_device.h
 * 
 * @returns {int} 0 if param is not an error code, 1 if it is an error code
 * 
 * Prints the message relative to the error code passed as param.
*/
int interpret_error(int code);

/**
 * @param {disk_block_t*} block
 * 
 * Prints the position (cylinder, head, sector) of the given block.
*/
void print_block(disk_block_t * block);

/**
 * @param {disk_repr_t*} disk
 * 
 * Prints the geometry (max cylinder, max head, max sector) of the given disk.
*/
void print_geometry(disk_repr_t * disk);

/**
 * @param {disk_repr_t*} disk
 * 
 * Opens a dialogue via terminal, asking for a block's position
 * and data in order to write safely by calling `swblock`.
*/
void action_safe_write(disk_repr_t * disk);

/**
 * @param {disk_repr_t*} disk
 * 
 * Opens a dialogue via terminal, asking for a block's position
 * and data in order to write by calling `wblock`.
*/
void action_write(disk_repr_t * disk);

/**
 * @param {disk_repr_t*} disk
 * 
 * Opens a dialogue via terminal, asking for
 * data in order to append a new block by calling `ablock`.
*/
void action_append(disk_repr_t * disk);

/**
 * @param {disk_repr_t*} disk
 * 
 * Opens a dialogue via terminal, asking for a block's position
 * in order to delete it by calling `dblock`.
*/
void action_delete(disk_repr_t * disk);

/**
 * @param {disk_repr_t*} disk
 * 
 * Opens a dialogue via terminal, asking for a block's position
 * and data in order to update it by calling `ublock`.
*/
void action_update(disk_repr_t * disk);

/**
 * @param {disk_repr_t*} disk
 * 
 * Opens a dialogue via terminal, asking for a block's position
 * in order to safely read it by calling `srblock`.
*/
void action_safe_read(disk_repr_t * disk);

/**
 * @param {disk_repr_t*} disk
 * 
 * Opens a dialogue via terminal, asking for a block's position
 * in order to read it by calling `rblock`.
*/
void action_read(disk_repr_t * disk);

/**
 * @param {disk_repr_t*} disk
 * 
 * Prints the number of allocated blocks.
*/
void action_count_blocks(disk_repr_t * disk);

/**
 * @param {disk_repr_t*} disk
 * 
 * Performs a disk scan and prints each allocated block position.
*/
void action_scan_disk(disk_repr_t * disk);

/**
 * @param {disk_repr_t*} disk
 * 
 * @returns {int} 1 if invalid input, 0 otherwise
 * 
 * Opens a dialogue asking for a block's position.
 * Used in almost all action functions.
*/
int ask_location(disk_repr_t * disk, disk_block_t * dest_block);

/**
 * @param {disk_repr_t*} disk
 * @param {char**} data
 * 
 * @returns {int} 0 everytime, implemented for scalability and compatibility with `ask_location`
 * 
 * Opens a dialogue asking for data to store.
*/
int ask_data(disk_repr_t * disk, char ** data);

/**
 * @param {disk_repr_t*} disk
 * 
 * @return {int} 0 if valid input, 1 otherwise
 * 
 * Opens a dialogue asking user for a new action to perform.
*/
int ask_action(disk_repr_t * disk);

#endif