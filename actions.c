#include "actions.h"
#include "stdterm.h"
#include "disk_device.h"

/* Functions are described in the header file. */

int parse_string_to_int(char *c, unsigned int length) {
    int result, i;
    result = 0;
    i = 0;

    for (; i < length; i++) {
        if (c[i] <= 57 && c[i] >= 48) {
            int zeroes = length - i;
            int tmp_res = c[i] - 48;
            for (; zeroes > 1; zeroes--) {
                tmp_res *= 10;
            }
            result += tmp_res;
        } else {
            break;
        }
    }

    for (; i < length; i++) {
        result /= 10;
    }

    return result;
}

void print_block(disk_block_t * b) {
    print("\n==================================================\n");
    print("Block:\n");
    print("Cylinder: ");
    print_int(b->cyl);
    print_char('\t');
    print("Head: ");
    print_int(b->head);
    print_char('\t');
    print("Sector: ");
    print_int(b->sect);
    print("\n==================================================\n");
}

void print_geometry(disk_repr_t * disk) {
    print("\n==================================================\n");
    print("Disk geometry:\n");
    print("Max cyl: ");
    print_int(disk->max_cyl);
    print_char('\n');
    print("Max head: ");
    print_int(disk->max_head);
    print_char('\n');
    print("Max sect: ");
    print_int(disk->max_sect);
    print("\n==================================================\n");
}

int interpret_error(int code) {
    int res = 1;
    if (code == -1) {
        print("DISK ERROR: Selected device is not installed.\n");
    } else if (code == -2) {
        print("DISK ERROR: Illegal operation.\n");
    } else if (code == -3) {
        print("DISK ERROR: Disk is currently busy.\n");
    } else if (code == -4) {
        print("DISK ERROR: Seek error.\n");
    } else if (code == -5) {
        print("DISK ERROR: Read error.\n");
    } else if (code == -6) {
        print("DISK ERROR: Write error.\n");
    } else if (code == -7) {
        print("DISK ERROR: DMA transfer error.\n");
    } else if (code == -8) {
        print("DISK ERROR: Block is not free.\n");
    } else if (code == -9) {
        print("DISK ERROR: Block is not used.\n");
    } else if (code == -10) {
        print("DISK ERROR: No space left.\n");
    } else {
        res = 0;
    }
    return res;
}

int ask_location(disk_repr_t * disk, disk_block_t * dest_block) {
    int res = 0;

    char * s_cylinder = "nnnnn";
    char * s_head = "nnn";
    char * s_sector = "nnn";

    print("\n==================================================\n");
    print("Where do you want to perform your action?\n");
    print("Cylinder: ");
    read_string(s_cylinder, 6);
    dest_block->cyl = parse_string_to_int(s_cylinder, 5);

    if (dest_block->cyl > disk->max_cyl) {
        res = 1;
        print("Cylinder location does not exist.\n");
        return res;
    }

    print("Head: ");
    read_string(s_head, 4);
    dest_block->head = parse_string_to_int(s_head, 3);

    if (dest_block->head > disk->max_head) {
        res = 1;
        print("Head location does not exist.\n");
        return res;
    }

    print("Sector: ");
    read_string(s_sector, 4);
    dest_block->sect = parse_string_to_int(s_sector, 3);

    if (dest_block->sect > disk->max_sect) {
        res = 1;
        print("Sector location does not exist.\n");
        return res;
    }

    return res;
}

int ask_data(disk_repr_t * disk, char ** data) {
    int res = 0;
    print("Data: ");
    read_string(*data, 256);
    return res;
}

void action_safe_write(disk_repr_t * disk) {
    int incorrect_input = 1;
    disk_block_t block = { 0, 0, 0 };
    char * data = "";
    int code = 0;

    while (incorrect_input) {
        incorrect_input = ask_location(disk, &block);
    }

    incorrect_input = 1;
    while (incorrect_input) {
        incorrect_input = ask_data(disk, &data);
    }

    code = swblock(disk, &block, data, 32);
    interpret_error(code);
}

void action_write(disk_repr_t * disk) {
    int incorrect_input = 1;
    disk_block_t block = { 0, 0, 0 };
    char * data = "";
    int code = 0;

    while (incorrect_input) {
        incorrect_input = ask_location(disk, &block);
    }

    incorrect_input = 1;
    while (incorrect_input) {
        incorrect_input = ask_data(disk, &data);
    }

    code = wblock(disk, &block, data, 32);
    interpret_error(code);
}

void action_append(disk_repr_t * disk) {
    int incorrect_input = 1;
    disk_block_t block = { 0, 0, 0 };
    char * data = "";
    int code = 0;

    incorrect_input = 1;
    while (incorrect_input) {
        incorrect_input = ask_data(disk, &data);
    }

    code = ablock(disk, &block, data, 32);
    if (!interpret_error(code)) {
        print("Your data has been saved at the following coordinates:\n");
        print_block(&block);
    }
}

void action_delete(disk_repr_t * disk) {
    int incorrect_input = 1;
    disk_block_t block = { 0, 0, 0 };
    int code = 0;

    while (incorrect_input) {
        incorrect_input = ask_location(disk, &block);
    }

    code = dblock(disk, &block);
    interpret_error(code);
}

void action_update(disk_repr_t * disk) {
    int incorrect_input = 1;
    disk_block_t block = { 0, 0, 0 };
    char * data = "";
    int code = 0;

    while (incorrect_input) {
        incorrect_input = ask_location(disk, &block);
    }

    incorrect_input = 1;
    while (incorrect_input) {
        incorrect_input = ask_data(disk, &data);
    }

    code = ublock(disk, &block, data, 32);
    interpret_error(code);
}

void action_safe_read(disk_repr_t * disk) {
    int incorrect_input = 1;
    disk_block_t block = { 0, 0, 0 };
    int code = 0;
    char * results = "";

    while (incorrect_input) {
        incorrect_input = ask_location(disk, &block);
    }

    code = srblock(disk, &block, results, 32);
    if (!interpret_error(code)) {
        print("\n==================================================\n");
        print(results);
        print("\n==================================================\n");
    };
}

void action_read(disk_repr_t * disk) {
    int incorrect_input = 1;
    disk_block_t block = { 0, 0, 0 };
    int code = 0;
    char * results = "";

    while (incorrect_input) {
        incorrect_input = ask_location(disk, &block);
    }

    code = rblock(disk, &block, results, 32);
    if (!interpret_error(code)) {
        print("\n==================================================\n");
        print(results);
        print("\n==================================================\n");
    };
}

void action_count_blocks(disk_repr_t * disk) {
    int code = 0;
    int blocks = cblocks(disk);

    if (!interpret_error(code)) {
        print("Number of used blocks: ");
        print_int(blocks);
        print_char('\n');
    }
}

void action_scan_disk(disk_repr_t * disk) {
    int n_blocks = cblocks(disk);
    disk_block_t blocks[n_blocks];

    n_blocks = scan_disk(disk, blocks, n_blocks);

    print("Retreived ");
    print_int(n_blocks);
    print(" blocks.\n");

    int i = 0;
    for (; i < n_blocks; i++) {
        print_block(&blocks[i]);
    }
}

int ask_action(disk_repr_t * disk) {
    int res = 0;
    int choice = 0;

    print("\n==================================================\n");
    print("Choose the action you want to perform on the disk:\n");
    print("0. Safely write a block\n");
    print("1. Write a block\n");
    print("2. Append a block\n");
    print("3. Update a block\n");
    print("4. Delete a block\n");
    print("5. Safely read a block\n");
    print("6. Read a block\n");
    print("7. Count used blocks\n");
    print("8. Get disk geometry\n");
    print("9. Scan disk\n\n");
    print("Action: ");

    choice = read_digit();
    read_char();

    if (choice == 0) {
        action_safe_write(disk);
    } else if (choice == 1) {
        action_write(disk);
    } else if (choice == 2) {
        action_append(disk);
    } else if (choice == 3) {
        action_update(disk);
    } else if (choice == 4) {
        action_delete(disk);
    } else if (choice == 5) {
        action_safe_read(disk);
    } else if (choice == 6) {
        action_read(disk);
    } else if (choice == 7) {
        action_count_blocks(disk);
    } else if (choice == 8) {
        print_geometry(disk);
    } else if (choice == 9) {
        action_scan_disk(disk);
    } else {
        res = 1;
    }

    return res;
}

