#ifndef _H_STDTERM_
#define _H_STDTERM_

#define TERM_READY 1
#define TERM_BUSY 3
#define TERM_CHAR_RECVD_TRSMD 5
#define TERM_CMD_ACK 1
#define TERM_CMD_TRNS_RCV 2

/**
 * Prints the given char to the terminal.
*/
int print_char(char c);

/**
 * Prints the given string to the terminal.
*/
int print(char * c);

/**
 * Prints the given int to the terminal.
*/
int print_int(int i);

/**
 * Waits for Enter key press then reads the last char from the terminal.
 * 
 * WARNING: Does not consume Enter key, it can still be read by another `read_char`!
*/
int read_char();

/**
 * Waits for Enter key press then reads the last char from the terminal and subtracts 48.
 * 
 * WARNING: Does not consume Enter key, it can still be read by another `read_char`!
*/
int read_digit();

/**
 * @param {char*} s : string in which to store input
 * @param {int} bound : maximum string length
 * 
 * @returns {int}
 * 
 * Reads input stream until it reaches bound or until it reads new line.
 * 
 * WARNING: Consumes Enter key only if it finishes before reaching bound!
*/
int read_string(char *s, int bound);

#endif