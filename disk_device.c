/*
    TODOs:
        - both proxy functions and real functions make the same
        checks (disk installed, disk readt, etc.), need to remove
        redundancy.
*/
#include "disk_device.h"
#include "umps/types.h"
#include "umps/libumps.h"
#include "umps/arch.h"
#include "stdterm.h"
typedef unsigned int uint;

uint STATUS_DEV_NINST = 0;
uint STATUS_DEV_READY = 1;
uint STATUS_ILL_OPCODE = 2;
uint STATUS_DEV_BUSY = 3;
uint STATUS_SERR = 4;
uint STATUS_RERR = 5;
uint STATUS_WERR = 6;
uint STATUS_TERR = 7;

uint CMD_RESET = 0;
uint CMD_ACK = 1;
uint CMD_SEEK_CYL = 2;
uint CMD_READBLK = 3;
uint CMD_WRITEBLK = 4;

#define BLOCK_DATA_OFFSET 2 /* defines from which byte data is written into a physical block. starts from 0. */
char BLOCK_FREE_MAGIC_MUMBER[] = {0xf5, 0xee};
char BLOCK_USED_MAGIC_NUMBER[] = {0x80, 0x57};

/*
    This buffer is heavily used in physical blocks read and write
    operations.
    All lower level functions like __raw_write_block and __raw_read_block will
    read/store data from/to this buffer.
    **This buffer is necessary** as the disk device will read/write a whole
    4KB region of memory during read/write operations.
*/
#define BUFFER_SIZE 4096
char DATA_BUFFER[BUFFER_SIZE];

/*
    Returns a value representing whether the first two bytes
    in DATA_BUFFER correspond to a usable (free) block.
    Returns:
       - 1 if the two bytes flag equals the block free flag.
       - -1 if the two bytes equal garbage data.
       - 0 of the two bytes equal the block used flag.
*/
int __is_magic_number_free(){
    if(DATA_BUFFER[0] == BLOCK_FREE_MAGIC_MUMBER[0] && DATA_BUFFER[1] == BLOCK_FREE_MAGIC_MUMBER[1]){
        /* the block is free */
        return 1;
    }else if(DATA_BUFFER[0] == BLOCK_USED_MAGIC_NUMBER[0] && DATA_BUFFER[1] == BLOCK_USED_MAGIC_NUMBER[1]){
        /* the block is already allocated */
        return 0;
    }else{
        /* block's magic number is corrupted */
        return -1;
    }
}

/*
    Prints the status of the disk device register pointed by 'reg'.
    Used for debugging.
*/
void __print_statuses(dtpreg_t *reg){
    int status = reg->status;
    if(status == STATUS_DEV_BUSY){
        print("busy\n");
    }
    if(status == STATUS_DEV_NINST){
        print("ninst\n");
    }
    if(status == STATUS_DEV_READY){
        print("ready\n");
    }
    if(status == STATUS_ILL_OPCODE){
        print("ill_opcode\n");
    }
    if(status == STATUS_RERR){
        print("rerr\n");
    }
    if(status == STATUS_WERR){
        print("werr\n");
    }
    if(status == STATUS_TERR){
        print("terr\n");
    }
    if(status == STATUS_SERR){
        print("serr\n");
    }
    print("==============\n");
}

/*
    Put the data from 'data' to DATA_BUFFER.
    Note that the data will be written starting from the third 
    byte of DATA_BUFFER.
*/
int __put_data_in_buffer(char *data, int length){
    int i = BLOCK_DATA_OFFSET;
    for(; i - BLOCK_DATA_OFFSET < length && i - BLOCK_DATA_OFFSET < BUFFER_SIZE; i++){
        DATA_BUFFER[i] = data[i - BLOCK_DATA_OFFSET];
    }
    return i - BLOCK_DATA_OFFSET;
}

/*
    Seeks the cylinder of the disk relative to 'disk_reg'.
    Returns E_SEEK on error.
    Does not check whether the disk is busy or not before the
    seek operation.

*/
int __seek_cyl(dtpreg_t *disk_reg, uint cyl){
    int result = 0;
    /* align data in the register for a seek command*/
    disk_reg->command = (cyl & 0xffff) << 8 | (CMD_SEEK_CYL & 0xff);
    disk_reg->command = CMD_ACK & 0xff;
    while(disk_reg->status == STATUS_DEV_BUSY);
    if(disk_reg->status == STATUS_SERR){
        result = E_SEEK;
    }
    return result;
}

void __set_busy_mnumber(){
    DATA_BUFFER[0] = (char)BLOCK_USED_MAGIC_NUMBER[0];
    DATA_BUFFER[1] = (char)BLOCK_USED_MAGIC_NUMBER[1];
}

void __set_free_mnumber(){
    DATA_BUFFER[0] = (char)BLOCK_FREE_MAGIC_MUMBER[0];
    DATA_BUFFER[1] = (char)BLOCK_FREE_MAGIC_MUMBER[1];
}

/*
    Performs a WRITEBLK operation writing data contained
    in DATA_BUFFER.
    Returns the number of bytes copied in DATA_BUFFER 
    (the smallest between 'data_length' and DATA_BUFFER's size - BLOCK_DATA_OFFSET).
    A negative return value indicates an error occurred.
    NOTE: DOES NOT SEEK.
*/
int __raw_write_block(disk_repr_t * disk, disk_block_t* block, char *data, int data_length){
    dtpreg_t *disk_reg = (dtpreg_t*) DEV_REG_ADDR(IL_DISK, disk->disk_id);
    int result = __put_data_in_buffer(data, data_length);
    
    disk_reg->data0 = (uint)DATA_BUFFER;
    disk_reg->command = (block->head & 0xff) << 16 | (block->sect & 0xff) << 8 | (CMD_WRITEBLK & 0xff);
    while(disk_reg->status == STATUS_DEV_BUSY);
    disk_reg->command = CMD_ACK & 0xff;

    int status = disk_reg->status;
    if(status == STATUS_ILL_OPCODE){
        result = E_ILLOP;
    }else if(status == STATUS_TERR){
        result = E_DMAT;
    }
    else if(status == STATUS_WERR){
        result = E_WRITE;
    }
    return result;
}

/*
    Performs a READBLK disk operation putting data in
    DATA_BUFFER.
    0 is returned if all goes well.
*/
int __raw_read_block(disk_repr_t *disk, disk_block_t* block){
    int result = 0;
    dtpreg_t *disk_reg = (dtpreg_t*) DEV_REG_ADDR(IL_DISK, disk->disk_id);
    
    disk_reg->data0 = (uint)DATA_BUFFER;
    disk_reg->command = (block->head & 0xff) << 16 | (block->sect & 0xff) << 8 | CMD_READBLK;
    while(disk_reg->status == STATUS_DEV_BUSY);
    disk_reg->command = CMD_ACK & 0xff;
    
    int status = disk_reg->status;

    if(status == STATUS_RERR){
        result = E_READ;
    }else if(status == STATUS_TERR){
        result = E_DMAT;
    }
    return result;
}

/*
    A block iterator.
    Starts from 0,0,0 and goes on forever.
    Can be reset by passing a non zero value as reset.
    If a reset is required no increment takes place.
    Returns 1 as long as the current value is respecting disk's boundaries.
*/
int __iterate_blocks(disk_repr_t *disk, disk_block_t * cb, int reset){
    static disk_block_t counter;
    if(reset){
        counter.cyl = counter.head = counter.sect = -1;
    }else{
        counter.sect = (counter.sect + 1) % disk->max_sect;
        if(counter.sect == 0){
            counter.head = (counter.head + 1) % disk->max_head;
            if(counter.head == 0){
                counter.cyl = counter.cyl + 1;
            }
        }
        cb->cyl = counter.cyl;
        cb->head = counter.head;
        cb->sect = counter.sect;
    }
    return counter.cyl < disk->max_cyl;
}

int wblock(disk_repr_t * disk, disk_block_t* block, char *data, int data_length){
    int result = 0;
    dtpreg_t *disk_reg = (dtpreg_t*) DEV_REG_ADDR(IL_DISK, disk->disk_id);
    uint status = disk_reg->status;
    if(status == STATUS_DEV_READY){
        if(__seek_cyl(disk_reg, block->cyl)){
            return E_SEEK;
        }
        __set_busy_mnumber();
        result = __raw_write_block(disk, block, data, data_length);
    }else if (status == STATUS_DEV_BUSY){
        result = E_BUSY;
    }else if(status == STATUS_DEV_NINST){
        result = E_NOINST;
    }
    return result;
}

int swblock(disk_repr_t * disk, disk_block_t* block, char *data, int data_length){
    int result = 0;
    dtpreg_t *disk_reg = (dtpreg_t*) DEV_REG_ADDR(IL_DISK, disk->disk_id);
    uint status = disk_reg->status;
    if(status == STATUS_DEV_READY){
        if(__seek_cyl(disk_reg, block->cyl)){
            return E_SEEK;
        }
        result = __raw_read_block(disk, block);
        if(result < 0){
            return result;
        }else{
            /* is the block a free block? */
            if(__is_magic_number_free()){
                __set_busy_mnumber();
                result = __raw_write_block(disk, block, data, data_length);
            }else{
                return E_BNFR;
            
            }
        }
    }else if(status == STATUS_DEV_BUSY){
        result = E_BUSY;
    }else if(status == STATUS_DEV_NINST){
        result = E_NOINST;
    }
    return result;

}

int rblock(disk_repr_t *disk, disk_block_t* block, char *data_buffer, int max_length){
    int result = 0;
    dtpreg_t *disk_reg = (dtpreg_t*) DEV_REG_ADDR(IL_DISK, disk->disk_id);
    uint status = disk_reg->status;
    if(status == STATUS_DEV_READY){
        if(__seek_cyl(disk_reg, block->cyl)){
            return E_SEEK;
        }
        result = __raw_read_block(disk, block);
        if(!result){
            int i = 0;
            for(; i < max_length && i < BUFFER_SIZE; i++){
                data_buffer[i] = DATA_BUFFER[i + BLOCK_DATA_OFFSET];
            }
            result = i;
        }
    }else if(status == STATUS_DEV_BUSY){
        result = E_BUSY;
    }else if(status == STATUS_DEV_NINST){
        result = E_NOINST;
    }

    return result;
}

int srblock(disk_repr_t *disk, disk_block_t* block, char *data_buffer, int max_length){
    int result = 0;
    dtpreg_t *disk_reg = (dtpreg_t*) DEV_REG_ADDR(IL_DISK, disk->disk_id);
    uint status = disk_reg->status;
    if(status == STATUS_DEV_READY){
        if(__seek_cyl(disk_reg, block->cyl)){
            return E_SEEK;
        }
        result = __raw_read_block(disk, block);
        if(!result){
            if(!__is_magic_number_free()){
                int i = 0;
                for(; i < max_length && i < BUFFER_SIZE; i++){
                    data_buffer[i] = DATA_BUFFER[i + 2];
                }
                result = i;
            }else{
                result = E_BNUS;
            }
        }
    }else if(status == STATUS_DEV_BUSY){
        result = E_BUSY;
    }else if(status == STATUS_DEV_NINST){
        result = E_NOINST;
    }

    return result;
}

int ablock(disk_repr_t *disk, disk_block_t *coordinates, char *data, int data_length){
    int result = 0;
    disk_block_t current_block = {0, 0, 0, 0};
    int found = 0;

    /* iterate all blocks on the disk until a non used one is found */
    __iterate_blocks(disk, &current_block, 1);
    while(__iterate_blocks(disk, &current_block, 0)){
        result = __raw_read_block(disk, &current_block);
        if(!result){
            if(__is_magic_number_free()){
                __set_busy_mnumber();
                result = __raw_write_block(disk, &current_block, data, data_length);
                if(result > 0){
                    coordinates->sect = current_block.sect;
                    coordinates->head = current_block.head;
                    coordinates->cyl = current_block.cyl;
                    found = 1;
                    break;
                }else{
                    return E_WRITE;
                }
            }
        }else{
            return result;                
        }
    }
    if(!found){
        result = E_NSPC;
    }
    return result;
}

void init_disk(disk_repr_t *disk){
    dtpreg_t *disk_reg = (dtpreg_t*) DEV_REG_ADDR(IL_DISK, disk->disk_id);
    disk_reg->command = CMD_ACK & 0xff;
    disk->max_cyl = (disk_reg->data1 & 0xffff0000) >> 16;
    disk->max_head = (disk_reg->data1 & 0x0000ff00) >> 8;
    disk->max_sect = disk_reg->data1 & 0xff;
}

int dblock(disk_repr_t * disk, disk_block_t * block){
    int result = 0;
    dtpreg_t *disk_reg = (dtpreg_t*) DEV_REG_ADDR(IL_DISK, disk->disk_id);
    int status = disk_reg->status;
    if(status == STATUS_DEV_READY){
        if(__seek_cyl(disk_reg, block->cyl)){
            result = E_SEEK;
        }else{
            __set_free_mnumber();
            result = __raw_write_block(disk, block, BLOCK_FREE_MAGIC_MUMBER, 2);
        }
    }else if(status == STATUS_DEV_BUSY){
        result = E_BUSY;
    }else if(status == STATUS_DEV_NINST){
        result = E_NOINST;
    }
    return result;
}

int ublock(disk_repr_t * disk, disk_block_t* to_update, char *data, int data_length){
    int result = 0;
    dtpreg_t *disk_reg = (dtpreg_t*) DEV_REG_ADDR(IL_DISK, disk->disk_id);
    int status = disk_reg->status;
    if(status == STATUS_DEV_READY){
        result = __seek_cyl(disk_reg, to_update->cyl);
        if(!result){
            result = __raw_read_block(disk, to_update);
            if(!__is_magic_number_free()){
                if(!result){
                    result = __raw_write_block(disk, to_update, data, data_length);
                }
            }else{
                result = E_BNUS;
            }
        }
    }else if(status == STATUS_DEV_BUSY){
        result = E_BUSY;
    }else if(status == STATUS_DEV_NINST){
        result = E_NOINST;
    }
    return result;
}

int cblocks(disk_repr_t *disk){
    int result = 0;
    dtpreg_t *disk_reg = (dtpreg_t*) DEV_REG_ADDR(IL_DISK, disk->disk_id);
    disk_block_t current_block = {0, 0, 0, 0};
    __iterate_blocks(disk, &current_block, 1); /* initialize iterator */
    while(__iterate_blocks(disk, &current_block, 0)){
        if(!__seek_cyl(disk_reg, current_block.cyl)){
            if(!__raw_read_block(disk, &current_block)){
                if(!__is_magic_number_free()){
                    /*  If we got to here without errors and the current block 
                        is non free then we can increment the block count.
                    */
                    result++;
                }
            }else{
                return E_READ;
            }
        }else{
            return E_SEEK;
        }
    }
    return result;
}

int scan_disk(disk_repr_t *disk, disk_block_t blocks[], int max_blocks){
    int result = 0;
    dtpreg_t *disk_reg = (dtpreg_t*) DEV_REG_ADDR(IL_DISK, disk->disk_id);
    int status = disk_reg->status;
    if(status == STATUS_DEV_READY){
        disk_block_t current;
        __iterate_blocks(disk, &current, 1); /* sets the iterator to 0 */
        while(__iterate_blocks(disk, &current, 0) && result < max_blocks){
            if(!__seek_cyl(disk_reg, current.cyl)){
                if(!__raw_read_block(disk, &current)){
                    if(!__is_magic_number_free()){
                        /* 
                            if we are here no read errors have occurred and
                            the current block is allocated.
                        */
                        blocks[result].cyl = current.cyl;
                        blocks[result].head = current.head;
                        blocks[result].sect = current.sect;
                        result++;
                    }
                }else{
                    return E_READ;
                }
            }else{
                return E_SEEK;
            }
        }
        
    }else if(status == STATUS_DEV_BUSY){
        result = E_BUSY;
    }else if(status == STATUS_DEV_NINST){
        result = E_NOINST;
    }
    return result;
}