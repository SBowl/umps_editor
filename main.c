#include "stdterm.h"
#include "disk_device.h"
#include "actions.h"

/**
 * Create your own disk file with `umps2-mkdev`,
 * use the new file as a device file for disk 0,
 * run the program, open teminal 0 and follow the steps
 * in order to complete the demo.
 * For more details about how the functions work
 * read the comments inside each header file.
 *
 * The demo is also described inside README.md
*/
int main(int argc, char const *argv[]) {
    /*
        The first part of the demo is just a simple example
        of how you would use the functions in disk_device.h
        in order to manage your disks.
    */

    char data[] = "hello world";
    disk_repr_t disk = {0, 0}; /* These values do not matter, as they will be changed in the next line. */
    init_disk(&disk); /* Disk is here initialized as a model of disk 0. */

    /* Printing disk geometry */
    print("Printing disk geometry...\n");
    print_geometry(&disk);

    /* Counting allocated blocks */
    print("\nCounting allocated blocks...\n");
    int res = cblocks(&disk);
    print("Found ");
    print_int(res);
    print(" allocated blocks.\n\n");

    /* Scanning disk and inserting allocated blocks inside an array */
    disk_block_t blocks[res];
    print("\nScanning disk...\n");
    int new_res = scan_disk(&disk, blocks, res);
    print("Added: ");
    print_int(new_res);
    print(" blocks.\n");
    print("First allocated block:");
    print_block(&blocks[0]);

    /* Appending a new block containing "hello world" to the disk */
    print("\nAppending \"hello world\"...\n");
    disk_block_t new_block;
    ablock(&disk, &new_block, data, 11);
    res = cblocks(&disk);
    print("Count after append: ");
    print_int(res);
    print_char('\n');
    print_char('\n');

    print("End of part one, have a glance at the results.\nPress Enter to continue with part two...\n");
    read_char();
    print_char('\n');

    /*
        In part two you can use the terminal in order to manage your disk.
        See actions.h for more details.
        We suggest starting with action 9 in order to have an overview of the disk.
    */
    while (1) {
        ask_action(&disk);
        print("Press Enter to continue...");
        read_char();
    }

    return 0;
}